"""
WSGI config for djambda project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os

import logging

logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.INFO)

logger.info("django wsgi...1")

from django.core.wsgi import get_wsgi_application

logger.info("django wsgi...2")
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'djambda.settings')
logger.info("django wsgi...3")
application = get_wsgi_application()
