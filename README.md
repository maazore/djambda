# Djambda

A serverless Django application built with AWS CDK and Lambda

## CDK

Start a new CDK python project with the following command:

```
cdk init app --language=python
```

Create a virtual environment for the CDK app:

```
python3 -m venv .env
```

Install dependencies for CDK from `setup.py`:

```
pip install -r requirements.txt
```

## Django

Create a new directory called `django` for the Django application

Create another venv for the Django project:

```
python3 -m venv .dj
```

Add a requirements file with Django:

```
Django==3.0
```

Install Django

```
pip install -r requirements.txt
```

Start a new Django project:

```
django-admin startproject djambda .
```

Create an app called `core`:

```
django-admin startapp core
```

Wire up a simple `JsonResponse` view to a path in `urls.py` in the `core` app.

```py
# djambda/urls.py
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('core.urls')),
]
```

```py
# core/urls.py
from django.http import JsonResponse

def base(request):
    return JsonResponse({"message": "OK"})
```

## GitLab

We can use GitLab to build our Lambda Layers so we don't have to commit our virtual env to source code.

We can use the `--target` (`-t`) option with pip to install Django dependencies to a location that we will specify in the Lambda Layer definition.

We could probably do the Django dependency and the `cdk deploy` in one step, but let's do it in two stages. This will require that we pass the files needed for the Lambda Layer between the two jobs using `artifacts`. Setup a `build` and `deploy` stage in GitLab CI:

## CDK

Now that we have our GitLab CI pipeline setup, we can start to build our CDK application that will deploy everything to AWS. We will come back to our Django Lambda handler function later and use a placeholder handler function for now.

Here are the AWS resources resources we'll use:

- Lambda
- API Gateway

We will start with these two, and then later work in some additional resources:

- VPC
- RDS
- S3
- Others

Let's follow along with the example from this CDK tutorial: [https://cdkworkshop.com/30-python/30-hello-cdk/300-apigw.html](https://cdkworkshop.com/30-python/30-hello-cdk/300-apigw.html).

## Putting Django into Lambda

Let's reference `handler.py` in the Zappa project to see how they do it.

Let's start with imports. There are three imports from `zappa`:

- `middleware`
- `wsgi`
- `utilities`

### `from zappa.middleware import ZappaWSGIMiddleware`

This middleware wraps the `wsgi_app_function` in `handler.py`. It looks like this Middleware mostly deals with Cookies. We will probably want to include this in our Djambda implementation. Here's the `test_handler` we can start with:

```py
import json
import os


def handler(event, context):
    try:
        import django
    except:
        print("could not import django")

    print(os.listdir("/opt"))
    print('request: {}'.format(json.dumps(event)))
    return {
        'statusCode': 200,
        'headers': {'Content-Type': 'text/plain'},
        'body': 'Hello, CDK! You have hit {}\n'.format(event['path']),
    }

```

### `wsgi`

`create_wsgi_request` is an important function. Here's the doc string for this function:

> Given some event_info via API Gateway, create and return a valid WSGI request environ.

This function uses a few utilities from Zappa.

- Checks `multiValueQueryStringParameters` for ALB/API Gateway compatability

- Builds `environ`

### Lambda Layers

To work with lambda layers, we have to coordinate a few directories and commands.

```py
        self.djambda_layer = _lambda.LayerVersion(
            self,
            "DjambdaLayer",
            code=_lambda.AssetCode("../django/.dj"),
            compatible_runtimes=[
                _lambda.Runtime.PYTHON_3_8,
                _lambda.Runtime.PYTHON_3_7,
            ],
        )
```

Locally, we need to specify the target with `-t`. In CI/CD this would be similar.

```
pip install -r requirements.txt -t .dj/python
```

For now we will be deploying changes locally as we debug the Django Lambda. When deploying locally, be sure not to inlcude the Python virtual environment for the Django application in the Lambda's `AssetCode`.

## Proxy Lambda

In order for the Djambda Lambda to access an RDS database, it needs to be deployed into a VPC. However, this will mean that the Lambda will not have internet access without a NAT Gateway in our VPC on a public subnet. To get around the need for a NAT Gateway, we can use another Lambda outside of our VPC that proxies requests to our Djambda Lambda in our VPC. The proxy lambda will invoke the Djambda Lambda and return the response from the Djambda Lambda to the client making the request.

Here's the code for the proxy Lambda:

```py
import json
import os
import boto3

lambda_client = boto3.client('lambda', region_name='us-east-1')


def handler(event, context):
    invoke_response = lambda_client.invoke(
        FunctionName=os.environ.get("FUNCTION_NAME", None),
        InvocationType='RequestResponse',
        Payload=json.dumps(event),
    )

    data = invoke_response['Payload'].read()

    return data

```

`FunctionName` is the name of the Djambda Lambda. This might change if you had multiple environments, so it is read in as an environment variable that is set in the proxy Lambda's definition in CDK:

```py
        self.djambda_layer = _lambda.LayerVersion(
            self,
            "DjambdaLayer",
            code=_lambda.AssetCode("./django/.dj"),
            compatible_runtimes=[_lambda.Runtime.PYTHON_3_7,],
        )

        self.djambda_lambda = _lambda.Function(
            self,
            "DjambdaLambda",
            runtime=_lambda.Runtime.PYTHON_3_7,
            code=_lambda.AssetCode('./django'),
            function_name="djambda_lambda",
            handler="handler.handler",
            layers=[self.djambda_layer],
            timeout=core.Duration.seconds(15),
            vpc=self.vpc,
        )

        self.proxy_lambda = _lambda.Function(
            self,
            "ProxyLambda",
            code=_lambda.AssetCode("./awslambda"),
            runtime=_lambda.Runtime.PYTHON_3_7,
            layers=[self.djambda_layer],
            handler="proxy_lambda.handler",
            timeout=core.Duration.seconds(15),
            environment={"FUNCTION_NAME": self.djambda_lambda.function_name},
        )
```

`InvocationType='RequestResponse',` is important; this won't work if the `InvocationType` is `Event`.

## Running Django management commands in Lambdas

## Using CloudFront to forward requests to API Gateway

Reading:

- [https://advancedweb.hu/how-to-use-api-gateway-with-cloudfront/](https://advancedweb.hu/how-to-use-api-gateway-with-cloudfront/)

CloudFront

## NAT Instance vs NAT Gateway

- NAT Gateway vs. Instance comparison: [https://www.theguild.nl/cost-saving-with-nat-instances/](https://www.theguild.nl/cost-saving-with-nat-instances/)
- T3a instances: [https://aws.amazon.com/ec2/instance-types/t3/](https://aws.amazon.com/ec2/instance-types/t3/)
- NAT instance images: [https://docs.aws.amazon.com/cdk/api/latest/python/aws_cdk.aws_ec2/NatInstanceImage.html](https://docs.aws.amazon.com/cdk/api/latest/python/aws_cdk.aws_ec2/NatInstanceImage.html)

## API Gateway Custom Domain

- [https://forums.aws.amazon.com/thread.jspa?threadID=269585&tstart=0](https://forums.aws.amazon.com/thread.jspa?threadID=269585&tstart=0)

## Questions

- What is the difference between an ISOLATED and PRIVATE subnet in a VPC?
- In the VPC CDK construct, why can't you use private subnets when you set AZs to 0?

- [ ] Can CloudFront forward to API Gateway? `/api/`, `/admin/`, etc. routes can be forwarded to `api.env.mysite.com` (the API Gateway custom domain)

## TODO

- Setup CloudFront distribution for S3 and API Gateway routing

- [x] `/static/` and `/media/` route to S3 Origin
- [x] `/api/`, `/admin/` and any other backend requests go to API Gateway custom domain
- [x] Default behavior will route to S3 static site, or possibly lambda@edge origin for serverless side rendering?
- [ ] Add option to keep lambdas warm: [https://read.acloud.guru/how-to-keep-your-lambda-functions-warm-9d7e1aa6e2f0](https://read.acloud.guru/how-to-keep-your-lambda-functions-warm-9d7e1aa6e2f0)
- [x] Add management commands in GitLab CI using aws cli to invoke lambdas

## RDS

- [x] Add [`aws-psycopg2`](https://pypi.org/project/aws-psycopg2/) to Django app requirements
- [x] Add `DatabaseCluster` consturct
- [x] Set override to add `EngineMode: serverless`
- [x] Configure security groups to allow the lambda security group access to the database security group
- [x] Put Database in ISOLATED subnet
- [x] Add environment variables that the Lambda functions can share
- [x] Add migrate and createsuperuser functions with triggers to call them through GitLab CI or invoked through AWS CLI locally (reuse the original lambda, but invoke with `manage` defined in the event so it triggers the calling of management command, not the processing of a request)

Run migrations with the following command:

```bash
aws lambda invoke --function-name dev-mysite-com-djambda-lambda --invocation-type RequestResponse --payload '{"manage": "migrate --no-input"}' resp.json
{
    "ExecutedVersion": "$LATEST",
    "StatusCode": 200
}
```

Run `createsuperuser` with:

```bash
aws lambda invoke --function-name dev-mysite-com-djambda-lambda --invocation-type RequestResponse --payload '{"manage": "createsuperuser --no-input --username admin --email brian@email.com"}' resp1.json
```

## S3

- [https://stackoverflow.com/questions/39779962/access-aws-s3-from-lambda-within-vpc](https://stackoverflow.com/questions/39779962/access-aws-s3-from-lambda-within-vpc)

- [x] Give Lambda access to S3 assets bucket
- [x] Create a VPC Gateway endpoint so that the VPC Lambda function can access S3 from S3
